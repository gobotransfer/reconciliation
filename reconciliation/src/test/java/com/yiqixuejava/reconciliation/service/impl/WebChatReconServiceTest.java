package com.yiqixuejava.reconciliation.service.impl;

import com.alibaba.fastjson.JSON;
import com.yiqixuejava.reconciliation.dto.RDetail;
import com.yiqixuejava.reconciliation.entity.SourceEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 类的描述：
 *
 * @author: huxp
 * @createDate: 2019/11/12 10:04
 * @version: v1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WebChatReconServiceTest {
    @Autowired
    WebChatReconService webChatReconService;

    @Test
    public void test() {
        SourceEntity sourceEntity = new SourceEntity();
        sourceEntity.setUri("1510339861All2019-10-29.csv");
        try {
            webChatReconService.prepare(sourceEntity);
            do {
                List<RDetail> rDetails = webChatReconService.getNextData(10);
                if (null == rDetails) {
                    break;
                }
                System.out.println("微信获取到" + rDetails.size() + "条数据");
                System.out.println("数据：" + JSON.toJSONString(rDetails));
            } while (true);
        } finally {
            webChatReconService.destroy();
        }

    }
}