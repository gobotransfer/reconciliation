package com.yiqixuejava.reconciliation.controller;

import com.yiqixuejava.reconciliation.JDBC.JDBCService;
import com.yiqixuejava.reconciliation.JDBC.SqlService;
import com.yiqixuejava.reconciliation.service.RApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 类的描述：对账控制类
 *
 * @author: like
 * @createDate: 2019年11月08日  10:50
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@RestController
public  class RController  {

    @Autowired
    private RApi rApi;
    @RequestMapping(value = "/test",method = RequestMethod.GET)
    public void test(){
        rApi.reconciliation("Alipy","Alipy");

    }

}