package com.yiqixuejava.reconciliation.controller.api;

import com.yiqixuejava.reconciliation.dto.UploadFileDto;
import com.yiqixuejava.reconciliation.util.Rsp;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * 类的描述：文件上传api
 *
 * @author: like
 * @createDate: 2019年11月08日  9:34
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@RequestMapping(value = "/upload")
public interface UploadApi {


    /**
     *  文件上传接口 此接口针对文件类型
     * @param batchNo 批次号（平台方和渠道方批次号应该一直）
     * @param filePath 文件路径（比如c://）
     * @param fileType 文件类型（如：ACOMA）
     * @param orgNo 机构编号（如：01）
     * @param flag 平台：P 渠道 Q
     * @param file 具体的文件
     * @return 成功之后返回一个具体的文件路径和文件名称
     */
    @RequestMapping(value = "/file",method = RequestMethod.POST)
    public Rsp<UploadFileDto> uploadImg(@NotNull  String batchNo, String filePath, @NotNull  String fileType, @NotNull  String orgNo, @NotNull  String flag, @RequestParam("file") MultipartFile file);

}