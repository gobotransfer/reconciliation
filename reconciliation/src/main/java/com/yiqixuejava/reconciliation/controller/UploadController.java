package com.yiqixuejava.reconciliation.controller;

import com.yiqixuejava.reconciliation.dto.UploadFileDto;
import com.yiqixuejava.reconciliation.controller.api.UploadApi;
import com.yiqixuejava.reconciliation.util.Rsp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * 类的描述：文件上传，此接口实现文件上传功能
 *
 * @author: like
 * @createDate: 2019年11月08日  9:33
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Slf4j
@RestController
public class UploadController implements UploadApi {

    public final static String CHARSETNAME = "UTF-8";
    /**
     * 文件名分割符
     */
    public final static String FILE_NAME_SEPARATOR  = "_";
    /**
     * 文件上传接口 此接口针对文件类型
     *
     * @param batchNo  批次号（平台方和渠道方批次号应该一直）
     * @param filePath 文件路径（比如c://）
     * @param fileType 文件类型（如：ACOMA）
     * @param orgNo    机构编号（如：01）
     * @param flag     平台：P 渠道 Q
     * @param file     具体的文件
     * @return 成功之后返回一个具体的文件路径和文件名称
     */
    @Override
    public Rsp<UploadFileDto> uploadImg(@NotNull String batchNo, String filePath, @NotNull  String fileType, @NotNull  String orgNo, @NotNull  String flag, @RequestParam("file") MultipartFile file)  {
        Rsp<UploadFileDto> uploadFileDtoRsp = new Rsp<>();
        UploadFileDto uploadFileDto = new UploadFileDto();
        try{
            // TODO 做文件格式的校验，只做系统能做的文件格式
            byte[] bytes = file.getBytes();
            String fileName = new String(file.getOriginalFilename().getBytes(CHARSETNAME));
            log.info("文件原始名字 name:"+ fileName +",文件size:"+file.getSize());
            fileName = getFileName(batchNo,fileType,orgNo,flag);
            uploadFileDto.setName(fileName);

            File fileDir = new File(filePath + File.separator + batchNo + File.separator);
            if(!fileDir.exists()){
                fileDir.mkdirs();
            }
            //得到原始文件的输入流
            InputStream inputStream=file.getInputStream();
            //得到新的文件的输出流
            String realFilePath = fileDir.getPath() + File.separator + fileName;
            uploadFileDto.setUrl(realFilePath);
            try (FileOutputStream fileOutputStream = new FileOutputStream(realFilePath)) {
                log.info("获取文件流" + fileOutputStream);
                fileOutputStream.write(bytes);
                fileOutputStream.flush();
                inputStream.close();
                fileOutputStream.close();
            }

            uploadFileDtoRsp.setData(uploadFileDto);
        }catch (Exception e){
            e.printStackTrace();
            return Rsp.error("10000001","文件上传错误");
        }

        return uploadFileDtoRsp;
    }

    /**
     * 获取文件名字 P_01_ACOM_0001
     * @param batchNo 批次号
     * @param fileType 文件类型
     * @param orgNo 机构号
     * @param flag 平台标志
     * @return
     */
    private String getFileName(String batchNo, String fileType, String orgNo, String flag) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(flag).append(FILE_NAME_SEPARATOR).append(fileType).append(FILE_NAME_SEPARATOR).append(orgNo).append(FILE_NAME_SEPARATOR).append(batchNo);
        return stringBuffer.toString();
    }
}