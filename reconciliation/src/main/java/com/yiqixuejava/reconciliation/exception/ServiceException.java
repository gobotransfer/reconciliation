package com.yiqixuejava.reconciliation.exception;

import lombok.Data;

/**
 * 类的描述：异常定义
 *
 * @author: like
 * @createDate: 2019年11月11日  10:25
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */

@Data
public class ServiceException extends RuntimeException {
    private String code;

    private String errorMessage;

    public ServiceException(String code, String errorMessage) {
        super(errorMessage);
        this.code = code;
        this.errorMessage = errorMessage;
    }

    public ServiceException(AbstractBaseExceptionEnum exception) {
        super(exception.getMessage());
        this.code = exception.getCode();
        this.errorMessage = exception.getMessage();
    }


}