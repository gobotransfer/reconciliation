
package com.yiqixuejava.reconciliation.exception;

/**
 * 异常规范
 *
 * @author: like
 * @createDate: 2019年11月11日  10:25
 */
public interface AbstractBaseExceptionEnum {

    /**
     * 获取异常的状态码
     */
    String getCode();

    /**
     * 获取异常的提示信息
     */
    String getMessage();
}
