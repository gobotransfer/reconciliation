
package com.yiqixuejava.reconciliation.exception;


/**
 * core模块的异常集合
 *
 * @author stylefeng
 * @Date 2018/1/4 22:40
 */
public enum CoreExceptionEnum implements AbstractBaseExceptionEnum {


    Unknown_EXCEPTION("500", "未知异常，请检查服务器代码"),
    BEAN_NOT_EXIST("501", "传入的解析bean不存在，请检查配置表中参数和配置serviceName"),
    LIST_SIZE_EXCEED_MAX("502", "传入数据量过大，请减少传入的数量，单次最大的数量为3000"),
    SOURCES_NOT_FOUND("503", "资源没有找到,请检查数据库配置"),
    JOB_IS_RUNNING("504", "当前任务正在对账，请稍后再试"),
    REDIS_INSERT_EXCEPTION("505", "redis服务插入异常,请检查redis配置"),
    REDIS_DIFF_EXCEPTION("506", "redis获取集合差集异常"),
    FILE_NOT_EXIST("1000", "读取文件错误，请检查文件是否存在"),
    NOT_RECON_DATA("6002", "获取数据为空"),
    NOT_MYSQL_DATA("6000", "传入数据缺失，请检查参数"),
    MAX_SIZE_NOT("6001", "获取数据条数不能为0"),

    FILE_PARSE_FAILED("1001", "文件解析失败"),
    Result_Is_Negative("1002","传入值为负数"),
    Error_Connection("1003","连接失败"),
    Not_Data("1004","数据库中没有数据");


    CoreExceptionEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    private String code;

    private String message;

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
