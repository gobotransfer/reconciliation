package com.yiqixuejava.reconciliation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiqixuejava.reconciliation.entity.SourceEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 类的描述：
 *
 * @author: like
 * @createDate: 2019年11月11日  16:47
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Mapper
public interface SourceMapper extends BaseMapper<SourceEntity> {
}