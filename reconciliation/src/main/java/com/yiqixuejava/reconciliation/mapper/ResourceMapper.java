package com.yiqixuejava.reconciliation.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yiqixuejava.reconciliation.entity.ResourceEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * 类的描述：
 *
 * @author: like
 * @createDate: 2019年11月08日  21:37
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Mapper
public interface ResourceMapper extends BaseMapper<ResourceEntity> {
    /**
     * 查询总共多少条
     * @param batchCountPSql
     * @return
     */
    long execBatchCountPSql(@Param(value = "batchCountPSql") String batchCountPSql);

    /**
     * 执行查询
     * @param batchSql
     * @return
     */
    Set<String> execBatchQuerySql(@Param(value = "batchSql") String batchSql);
}