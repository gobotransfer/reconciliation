package com.yiqixuejava.reconciliation.util;


import com.alibaba.fastjson.JSONObject;
import lombok.Getter;
import lombok.Setter;

/**
 * 公共响应封装类
 */

@Getter
@Setter
public class Rsp<T> {
	public static final String SUCCESS_CODE = "00000200";

	/**状态码***/
	private String code = "00000200";
	/****状态描述*****/
	private String msg = "成功";
	/*****响应对象******/
	private T data;



	public Rsp() {

	}

	public static Rsp success(){
		return new Rsp();
	}


	public Rsp(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public Rsp(String code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}



	/**
	 * 判断是否成功
	 * @param rsp 返回信息
	 * @return
	 */
	public static boolean isSuccess(Rsp rsp){
		if(SUCCESS_CODE.equals(rsp.getCode())){
			return true;
		}
		return  false;
	}
	
	public boolean successed() {
		return SUCCESS_CODE.equals(code);
	}

	@Override
	public String toString() {
		JSONObject rspJson=new JSONObject();
		rspJson.put("code", code);
		rspJson.put("msg", msg);
		rspJson.put("data", data);
		return rspJson.toJSONString();
	}
	
	public static <T> Rsp<T> ok(T data) {
		Rsp rsp = new Rsp();
		rsp.setData(data);
		return rsp;
	}
	
	public static <T> Rsp<T> error(String code, String msg) {
		Rsp rsp = new Rsp();
		rsp.setCode(code);
		rsp.setMsg(msg);
		return rsp;
	}
	
	public static Rsp ok () {
		return ok(null);
	}


	
	
}
