package com.yiqixuejava.reconciliation.util;


/**
 * 生成流水工具类
 */
public class CodeUtils {

    /**
     * 生成流水号
     * @param codePrefix 前缀
     * @return
     */
    public static String generateCode(String codePrefix) {
        StringBuilder sb = new StringBuilder();
        sb.append(codePrefix);
        final IdGenerator idg = IdGenerator.INSTANCE;
        sb.append( idg.nextId());
        return sb.toString();
    }

    public static String generateCode() {
        final IdGenerator idg = IdGenerator.INSTANCE;
        return idg.nextId();
    }

    public static Long generateId(){
        final IdGenerator idg = IdGenerator.INSTANCE;
        String id = idg.nextId();
        return Long.valueOf(id);
    }



//    public static void main(String[] args) {
//
//
//        Integer fpTotal = 4;
//        Integer myFb = 3;
//
//        System.out.println("每3s查询一次，查询40S 所有交易中的数据");
//        System.out.println("数据记录查询次数，订单状态，超过11次就撤销");
//
//
//        for (int i = 0; i <1000 ; i++) {
//            String id = generateCode();
//            System.out.println(id);
//            Integer sss = Integer.parseInt(id.substring(14,17));
//            System.out.println(sss);
//            Integer result = sss%fpTotal;
//            switch (result){
//                case 0:
//                    System.out.println("数据已被分片1执行查询");
//
//                    break;
//                case 1:
//                    System.out.println("数据已被分片2执行查询");
//                    break;
//                case 2:
//                    System.out.println("数据已被分片3执行查询");
//                    break;
//                case 3:
//                    System.out.println("数据已被分片4执行查询");
//                    break;
//
//
//
//            }
//        }
//    }

}
