package com.yiqixuejava.reconciliation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiqixuejava.reconciliation.entity.ResourceEntity;
import com.yiqixuejava.reconciliation.mapper.ResourceMapper;
import com.yiqixuejava.reconciliation.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * 类的描述：
 *
 * @author: like
 * @createDate: 2019年11月08日  21:35
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Service
public class ResourceServiceImpl extends ServiceImpl<ResourceMapper,ResourceEntity> implements ResourceService {

    @Autowired
    private ResourceMapper resourceMapper;
    /**
     * 执行查询语句，查询一共有多少条
     *
     * @param batchCountPSql 批量查询SQL
     * @return
     */
    @Override
    public long execBatchCountPSql(String batchCountPSql) {
        return resourceMapper.execBatchCountPSql(batchCountPSql);
    }

    /**
     * 执行查询语句，查询回来数据直接加载到redis
     *
     * @param batchSql
     * @return
     */
    @Override
    public Set<String> execBatchQuerySql(String batchSql) {
        return resourceMapper.execBatchQuerySql(batchSql);
    }
}