package com.yiqixuejava.reconciliation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yiqixuejava.reconciliation.entity.ResourceEntity;

import java.util.Set;

/**
 * 类的描述：
 *
 * @author: like
 * @createDate: 2019年11月08日  21:33
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
public interface ResourceService extends IService<ResourceEntity> {

    /**
     * 执行查询语句，查询一共有多少条
     * @param batchCountPSql 批量查询SQL
     * @return
     */
    public long execBatchCountPSql(String batchCountPSql);

    /**
     * 执行查询语句，查询回来数据直接加载到redis
     * @param batchPSql
     * @return
     */
    Set<String> execBatchQuerySql(String batchPSql);
}