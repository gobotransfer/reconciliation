package com.yiqixuejava.reconciliation.service;

/**
 * 类的描述：对账
 *
 * @author: like
 * @createDate: 2019年11月08日  10:47
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */

public interface RApi {

    /**
     * 对账
     * @param pBeanName 平台方资源beanName
     * @param qBeanName 渠道方资源beanName
     */
    void reconciliation(String pBeanName,String qBeanName);




}