package com.yiqixuejava.reconciliation.service.impl;

import com.yiqixuejava.reconciliation.dto.RDetail;
import com.yiqixuejava.reconciliation.entity.SourceEntity;
import com.yiqixuejava.reconciliation.exception.ServiceException;
import com.yiqixuejava.reconciliation.service.ReconService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 类的描述：
 *
 * @author: huxp
 * @createDate: 2019/11/11 17:14
 * @version: v1.0
 */
@Service("AlipayReconService")
@Scope("prototype")
public class AlipayReconService implements ReconService {
    private Logger log = LoggerFactory.getLogger(getClass());
    private static final String ALIPAY_FILE_CODE = "UTF-8";
    @Override
    public void prepare(SourceEntity sourceEntity) throws ServiceException {

    }

    @Override
    public List<RDetail> getNextData(int maxSize) {
        return null;
    }

    @Override
    public void destroy() {

    }
}
