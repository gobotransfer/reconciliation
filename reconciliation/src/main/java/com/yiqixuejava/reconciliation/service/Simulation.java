package com.yiqixuejava.reconciliation.service;

import com.yiqixuejava.reconciliation.entity.SourceEntity;
import com.yiqixuejava.reconciliation.exception.ServiceException;

/**
 * 类的描述：此接口主要功能是定义模拟生成文件或者数据库
 *
 * @author: like
 * @createDate: 2019年11月13日  9:36
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
public interface Simulation {

    /**
     * 生成模拟数据
     * @param sourceEntity 数据源
     * @return 成功生成还是失败
     * @throws ServiceException
     */
    public boolean simulation(SourceEntity sourceEntity) throws ServiceException;
}