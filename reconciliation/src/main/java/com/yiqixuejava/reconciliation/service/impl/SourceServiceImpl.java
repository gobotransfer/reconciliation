package com.yiqixuejava.reconciliation.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yiqixuejava.reconciliation.entity.SourceEntity;
import com.yiqixuejava.reconciliation.mapper.SourceMapper;
import com.yiqixuejava.reconciliation.service.SourceService;
import org.springframework.stereotype.Service;

/**
 * 类的描述：
 *
 * @author: like
 * @createDate: 2019年11月11日  16:48
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Service
public class SourceServiceImpl extends ServiceImpl<SourceMapper,SourceEntity> implements SourceService {
}