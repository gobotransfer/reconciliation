package com.yiqixuejava.reconciliation.service;

import com.yiqixuejava.reconciliation.dto.RDetail;
import com.yiqixuejava.reconciliation.entity.SourceEntity;
import com.yiqixuejava.reconciliation.exception.ServiceException;

import java.util.List;

/**
 * 类的描述：对账基础接口，实现方应该配置好自己的service,并把service的名字配置到资源表中
 *
 * @author: like
 * @createDate: 2019年11月11日  10:23
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
public interface ReconService {

    /**
     * 对账准备接口，实现方应该准备好对账所需资源
     * @throws ServiceException
     */
    void prepare(SourceEntity sourceEntity) throws ServiceException;

    /**
     * 获取对账数据
     * @return
     */
    List<RDetail> getNextData(int maxSize) throws  Exception;


    /**
     * 关闭资源接口，如关闭流或者数据库连接
     */
    void destroy();


}