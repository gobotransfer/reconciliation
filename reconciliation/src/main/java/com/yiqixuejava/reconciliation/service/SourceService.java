package com.yiqixuejava.reconciliation.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yiqixuejava.reconciliation.entity.SourceEntity;

/**
 * 类的描述：
 *
 * @author: like
 * @createDate: 2019年11月11日  16:48
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
public interface SourceService extends IService<SourceEntity> {
}