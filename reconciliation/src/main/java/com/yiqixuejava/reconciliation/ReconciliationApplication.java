package com.yiqixuejava.reconciliation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 统一对账系统启动类
 * @author like
 */
@Slf4j
@SpringBootApplication
public class ReconciliationApplication {

    public static void main(String[] args) {
        log.info("------开始启动统一对账系统-----");
        SpringApplication.run(ReconciliationApplication.class, args);
        log.info("------统一对账系统启动完成------");
    }

}
