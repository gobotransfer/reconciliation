package com.yiqixuejava.reconciliation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 类的描述：对账数据顺序
 *
 * @author: like
 * @createDate: 2019年11月11日  10:38
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RFiledName {

    /**
     * 对账顺序，默认参与顺序是从小到大
     * @return
     */
  public  int order() default 0;

}