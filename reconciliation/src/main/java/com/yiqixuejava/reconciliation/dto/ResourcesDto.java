package com.yiqixuejava.reconciliation.dto;

import java.util.List;

/**
 * 类的描述：解析之后的资源数据
 *
 * @author: like
 * @createDate: 2019年11月08日  21:04
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
public class ResourcesDto {
    private String resourcesId;
    private int size;
    private List<RDetail> detailList;

    public int getSize() {
        return size;
    }

    private void setSize() {
        this.size = detailList == null ? -1 : detailList.size();
    }

    public List<RDetail> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<RDetail> detailList) {
        this.detailList = detailList;
    }

    public String getResourcesId() {
        return resourcesId;
    }

    public void setResourcesId(String resourcesId) {
        this.resourcesId = resourcesId;
    }

    public void setSize(int size) {
        this.size = size;
    }
}