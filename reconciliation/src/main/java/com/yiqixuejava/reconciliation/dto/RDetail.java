package com.yiqixuejava.reconciliation.dto;

import com.yiqixuejava.reconciliation.annotation.RFiledName;
import lombok.Data;

/**
 * 类的描述：对账原始数据明细
 *
 * @author: like
 * @createDate: 2019年11月08日  21:06
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
public class RDetail {

    /**
     * 订单号
     */
    @RFiledName(order = 1)
    private String orderNo;
    /**
     * 交易金额 单位为分，不能有小数点
     */
    @RFiledName(order = 2)
    private String tradeAmount;
    /**
     * 交易时间 yyyy-MM-DD HH:mm:ss
     */
    private String tradeDateTime;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(String tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public String getTradeDateTime() {
        return tradeDateTime;
    }

    public void setTradeDateTime(String tradeDateTime) {
        this.tradeDateTime = tradeDateTime;
    }
}