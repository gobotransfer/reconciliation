package com.yiqixuejava.reconciliation.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 类的描述：文件上传成功之后响应对象
 *
 * @author: like
 * @createDate: 2019年11月08日  9:48
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Data
public class UploadFileDto implements Serializable {
    /**文件上传之后的地址**/
    private String url;
    /*****文件名字******/
    private String name;
}