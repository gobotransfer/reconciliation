package com.yiqixuejava.reconciliation.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 类的描述：来源实体
 *
 * @author: like
 * @createDate: 2019年11月11日  16:24
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Data
@TableName(value = "r_source")
public class SourceEntity implements Serializable {
    @TableId(type = IdType.AUTO)
    private int id;
    /**创建时间**/
    private Date createTime;
    /**版本号**/
    private String version;
    /**机构标识**/
    private String orgId;
    /**资源编码，应该和解析类注入spring bean name一致**/
    private String sourceCode;
    /**资源名称**/
    private String sourceName;
    /**资源URI**/
    private String uri;
    /**特殊参数，数据库是用户名**/
    private String param1;
    /**特殊参数，数据库是密码**/
    private String param2;
    /**特殊参数，数据库是驱动类**/
    private String param3;
    /**其他特殊参数，等待后续开发**/
    private String otherParams;


}