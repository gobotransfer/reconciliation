package com.yiqixuejava.reconciliation.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 类的描述：
 *
 * @author: like
 * @createDate: 2019年11月08日  21:28
 * @version: v1.0
 * @jdk version used: JDK1.8
 * @see <相关类>
 */
@Data
@TableName("r_resources")
public class ResourceEntity {
    @TableId(type = IdType.AUTO)
    private int id;
    private String batchNo;
    /**资源标识**/
    private String sourceCode;
    /**平台标识 平台 或者 渠道**/
    private String platform;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 交易金额 都为分，不能有小数点
     */
    private String tradeAmount;
    /**
     * 交易时间 yyyyMMDDHHmmss
     */
    private String tradeDateTime;
    /**
     * 对账结果 一致 0，平台多 1，渠道多 2
     */
    private int result;
}