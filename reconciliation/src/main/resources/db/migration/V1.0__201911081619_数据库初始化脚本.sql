/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/11/8 9:49:52                            */
/*==============================================================*/



drop table if exists r_source;
drop table if exists r_resources;
drop table if exists organization;





/*==============================================================*/
/* Table: r_source                                           */
/*==============================================================*/
CREATE TABLE `r_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '数据库主键',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `org_id` int(11) DEFAULT NULL COMMENT '机构ID',
  `version` varchar(10) NOT NULL DEFAULT '1.0' COMMENT '版本',
  `source_code` varchar(32) DEFAULT NULL COMMENT '文件来源编码',
  `source_name` varchar(32) DEFAULT NULL COMMENT '文件来源名称',
  `uri` varchar(300) DEFAULT NULL COMMENT '关联统一资源标识符',
  `param1` varchar(100) DEFAULT NULL COMMENT '参数1',
  `param2` varchar(100) DEFAULT NULL COMMENT '参数2',
  `param3` varchar(100) DEFAULT NULL COMMENT '参数3',
  `other_params` varchar(500) DEFAULT NULL COMMENT '其他参数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='文件来源';


/*==============================================================*/
/* Table: organization                                          */
/*==============================================================*/
create table organization
(
   id                   integer(11) not null comment '数据库主键',
   create_time          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   version              varchar(10) not null default '1.0' comment '版本',
   organization_code    varchar(32) comment '机构编码',
   organization_name    varchar(32) comment '机构名称',
   primary key (id)
);

alter table organization comment '机构表';

CREATE TABLE `r_resources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batch_no` varchar(32) NOT NULL COMMENT '批次号',
  `platform` varchar(10) NOT NULL COMMENT '渠道还是平台',
  `source_code` varchar(32) NOT NULL COMMENT '资源来源编码',
  `order_no` varchar(32) NOT NULL COMMENT '订单号',
  `trade_amount` int(11) NOT NULL COMMENT '交易金额',
  `trade_date_time` varchar(32) NOT NULL COMMENT '交易时间',
  `result` varchar(32) NOT NULL COMMENT '对账结果',
  PRIMARY KEY (`id`,`batch_no`,`platform`,`source_code`,`order_no`,`trade_amount`,`trade_date_time`,`result`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='统一入账元素';


alter table r_resources comment '统一入账元素';


